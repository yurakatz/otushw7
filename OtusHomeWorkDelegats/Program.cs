﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace OtusHomeWorkDelegats
{
   
    

    internal class Program
    {
        private static void Main(string[] args)
        {

            var rnd = new Random();

            var testis = new List<HelperClass>();
            for (var i = 0; i < 10000; i++)
                testis.Add(new HelperClass
                {
                    Floatval = (float)rnd.NextDouble(),
                    Intval = i
                });
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var max1 = testis.GetMaxV1(x => x.Floatval); // "min", 1
            stopwatch.Stop();
            Console.WriteLine($"GetMaxV1 Elapsed Time is {stopwatch.ElapsedMilliseconds} ms result:{max1.Intval}");

            stopwatch.Restart();
            var max2 = testis.GetMaxV2(x => x.Floatval); // "min", 1
            stopwatch.Stop();
            Console.WriteLine($"GetMaxV2 Elapsed Time is {stopwatch.ElapsedMilliseconds} ms result:{max2.Intval}");
        }
    }
}