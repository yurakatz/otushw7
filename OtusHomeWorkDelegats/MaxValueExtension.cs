﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OtusHomeWorkDelegats
{
    public static class MaxValueExtension
    {
        public static T GetMaxV1<T>(this IEnumerable<T> e, Func<T, float> getParametr) where T : class
        {
            return e.FirstOrDefault(a => getParametr(a) == e.Max(getParametr));
        }

        public static T GetMaxV2<T>(this IEnumerable<T> e, Func<T, float> getParametr) where T : class
        {
            return e.OrderByDescending(getParametr).First();
        }
    }
}