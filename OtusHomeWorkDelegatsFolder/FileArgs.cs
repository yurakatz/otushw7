﻿using System;

namespace OtusHomeWorkDelegatsFolder
{
    public class FileArgs : EventArgs
    {
        public FileArgs(string filename)
        {
            Filename = filename;
        }
        public string Filename { get;  }
    }
}