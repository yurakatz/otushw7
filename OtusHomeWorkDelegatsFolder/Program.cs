﻿using System;
using System.Threading.Tasks;

namespace OtusHomeWorkDelegatsFolder
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var test = new FileSearch();
            test.FileFound += (sender, fileArgs) => Console.WriteLine(fileArgs.Filename);
            Task.Run(() => { test.DirSearch(@"c:\"); });
            Console.WriteLine("Press x for stop process");

            ConsoleKeyInfo keyinfo;
            do
            {
                keyinfo = Console.ReadKey();
                Console.WriteLine(keyinfo.Key + " was pressed");
            } while (keyinfo.Key != ConsoleKey.X);

            Console.WriteLine("Stop request Detected");
            test.StopRequest += (sender, eventArgs) => Console.WriteLine();

            Console.ReadKey();
        }
    }
}