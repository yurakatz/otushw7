﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace OtusHomeWorkDelegatsFolder
{
    internal class FileSearch
    {
        public event EventHandler<FileArgs> FileFound;
        public event EventHandler StopRequest;

        public void DirSearch(string sDir)
        {
            try
            {
                foreach (var directory in Directory.GetDirectories(sDir))
                {
                    try
                    {
                        foreach (var filename in Directory.GetFiles(directory))
                        {
                            if (StopRequest != null)
                                return;
                            FileFound?.Invoke(this, new FileArgs(filename));
                            Task.Delay(1000).Wait();
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }


                    DirSearch(directory);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}